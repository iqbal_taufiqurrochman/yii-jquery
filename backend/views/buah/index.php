<?php
use yii\helpers\Url;
use yii\helpers\Html;
use backend\models\Buah;
$this->title = 'Buah Perjuangan, Huehehehe';
 ?>
<style media="screen">
    .well-white {
        background-color:white;
    }
</style>

<div class="angga" data="anu">

</div>

<button type="button" id="btnku" name="button">klikme</button>

<div class="well">
    <div class="well well-white">
        <b style="text-align: left">Buah-Buah an ya Gaes</b>
        <button style="margin-top : -5px" type="button" class="pull-right btn btn-success" id="btn-tambah" name="button">Tambah</button>
    </div>
    <div class="well well-white">
        <table class="table table-hover">
            <thead>
                <th>No</th>
                <th>Nama</th>
                <th>Jumlah</th>
                <th style="text-align: center">#</th>
            </thead>
            <tbody id="showdata">
                <!-- Tampil disini -->
            </tbody>
        </table>
        <div id="loading" style="text-align: center">
            <img src="http://127.0.0.1/yii-jquery/backend/web/images/loading.gif" style="width : 50px; height: 50px">
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-tambah">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Buah</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" name="nama" placeholder="Nama Buah nya apa?" value="">
        &nbsp;
        <input type="text" class="form-control" name="jumlah"placeholder="Jumlah nya berapa?" value="">
      </div>
      <div class="modal-footer">
        <button type="button" id="btn-simpan" class="btn btn-primary">Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php foreach (Buah::find()->all() as $value): ?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit<?= $value->id?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Buah</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" name="nama" placeholder="Nama Buah nya apa?" value="">
        &nbsp;
        <input type="text" class="form-control" name="jumlah"placeholder="Jumlah nya berapa?" value="">
      </div>
      <div class="modal-footer">
        <button type="button" id="btn-simpan" class="btn btn-primary">Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endforeach; ?>

<?php
    $this->registerJs('

        $("#btnku").click(function(){
            alert("")
        });

        show();
        function show(){
            $("#loading").show();
            $.ajax({
                type : "ajax",
                method : "post",
                url : "'.Url::to(["/buah/show"], true).'",
                dataType : "json",
                async : "false",
                success : function(data){
                    var html = "";
                    for(var i = 0 ; i < data.length; i++){
                        html += "<tr>"+
                            "<td>"+(i+1)+"</td>"+
                            "<td>"+data[i].nama+"</td>"+
                            "<td>"+data[i].jumlah+"</td>"+
                            "<td style=\"text-align: center\"><span id=\"btn-edit\" data-id="+data[i].id+" class=\"glyphicon glyphicon-pencil btn btn-primary btn-sm\" data-target=\"modal-edit\"></span>&nbsp;<span id=\"btn-hapus\" data-id="+data[i].id+" class=\"glyphicon glyphicon-trash btn btn-danger btn-sm\"></span></td>"+
                        "</tr>"
                    }
                    $("#showdata").html(html);
                },
                complete : function(){
                    $("#loading").hide();
                }
            });
        }

        $("#btn-tambah").click(function(){
            $("#modal-tambah").modal();
            $("#btn-simpan").click(function(){
                console.log("klik button simpan");
            });
        });

        $("#btn-edit").click(function(){

        });
    ');
 ?>
