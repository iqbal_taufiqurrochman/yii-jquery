<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "buah".
 *
 * @property int $id
 * @property string $nama
 * @property int $jumlah
 */
class Buah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'jumlah'], 'required'],
            [['jumlah'], 'integer'],
            [['nama'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'jumlah' => 'Jumlah',
        ];
    }
}
