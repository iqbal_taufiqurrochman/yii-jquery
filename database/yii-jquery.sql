-- Adminer 4.4.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `buah`;
CREATE TABLE `buah` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(15) NOT NULL,
  `jumlah` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `buah` (`id`, `nama`, `jumlah`) VALUES
(1,	'Apel',	3),
(2,	'Anggur',	7);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `nama`, `username`, `password`) VALUES
(1,	'admin',	'admin',	'21232f297a57a5a743894a0e4a801fc3');

-- 2019-05-18 01:01:51
